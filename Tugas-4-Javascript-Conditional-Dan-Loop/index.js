//Soal 1
var nilai = 99;

if(nilai >= 85){
    console.log('Indeks: A')
}
else if(nilai >= 75 && nilai < 85){
    console.log('Indeks: B')
}
else if(nilai >= 65 && nilai < 75){
    console.log('Indeks: C')
}
else if(nilai >= 55 && nilai < 65){
    console.log('Indeks: D')
}
else {
    console.log('Indeks: E')
}

//Soal 2
var tanggal = 22;
var bulan = 1;
var tahun = 1994;

switch(bulan){
    case 1: nama_bulan = "Januari";
        break;
    case 2: nama_bulan = "Februari";
        break;
    case 3: nama_bulan = "Maret";
        break;
    case 4: nama_bulan = "April";
        break;
    case 5: nama_bulan = "Mei";
        break;
    case 6: nama_bulan = "Juni"; 
        break;
    case 7: nama_bulan = "Juli";
        break;
    case 8: nama_bulan = "Augustus";
        break;
    case 9: nama_bulan = "September";
        break;
    case 10: nama_bulan = "Oktober";
        break;
    case 11: nama_bulan = "November";
        break;
    case 12: nama_bulan = "Desember";
        break;
}

console.log(tanggal+ ' '+ nama_bulan +' '+ tahun);

//Soal 3
var n = 5;
var segitiga = '';

for (var i = 0; i < n; i++) {
    for (var j = 0; j <= i; j++) {
        segitiga += '#';
    }
    segitiga += '\n';
}
console.log(segitiga);

//Soal 4
var kalimat = ['I love programming', 'I love javascript', 'I love vuejs'];
var y = 0;

var m = 10;

for(var x = 1; x <= m; x++){
    console.log(x + ' - '+kalimat[y])
    y+=1;

    if(y == kalimat.length){
        console.log("=".repeat(x));
        y = 0;
    }

}
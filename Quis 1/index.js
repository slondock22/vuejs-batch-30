//Soal 1
var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "


console.log(jumlah_kata(kalimat_1)) // 6
console.log(jumlah_kata(kalimat_2)) // 2
console.log(jumlah_kata(kalimat_3)) // 4

function jumlah_kata(kalimat) { 
    var a = kalimat.trim().split(" "); 
    return a.length; 
}

//Soal 2

var tanggal = 31
var bulan = 12
var tahun = 2020

console.log(next_date(tanggal, bulan, tahun))

function next_date(tanggal, bulan, tahun) {
    var max_tanggal = 0;
    if ((tanggal > 0) && (tanggal <= 31) && (bulan > 0) && (bulan <= 12) && (tahun > 0)) {
        if (Math.floor(bulan / 8) == 0) {
            console.log('a');
            if (bulan % 2 == 1) {
                max_tanggal = 31;
            } else {
                console.log('b');
                if (bulan == 2) {
                    console.log('c');
                    if (tahun % 4 == 0) {
                        console.log('d');
                        max_tanggal = 29;
                    } else {
                        console.log('e');
                        max_tanggal = 28;
                    }
                } else {
                    max_tanggal = 30;
                }
            }
        } else {
            if (bulan % 2 == 1) {
                max_tanggal = 30;
            } else {
                max_tanggal = 31;
            }
        }
        if (tanggal == max_tanggal) {
            if (bulan == 12) {
                tanggal = 1;
                bulan = 1;
                tahun = tahun + 1;
            } else {
                tanggal = 1;
                bulan = bulan + 1;
            }
        } else {
            tanggal = tanggal + 1;
        }
        if (tanggal > max_tanggal) {
            console.log('Tanggal Tersebut Pada Bulan atau Tahun Tersebut Tidak Ada');
        } else {
            switch (bulan) {
                case 1:
                    nama_bulan = "Januari";
                    break;
                case 2:
                    nama_bulan = "Februari";
                    break;
                case 3:
                    nama_bulan = "Maret";
                    break;
                case 4:
                    nama_bulan = "April";
                    break;
                case 5:
                    nama_bulan = "Mei";
                    break;
                case 6:
                    nama_bulan = "Juni";
                    break;
                case 7:
                    nama_bulan = "Juli";
                    break;
                case 8:
                    nama_bulan = "Augustus";
                    break;
                case 9:
                    nama_bulan = "September";
                    break;
                case 10:
                    nama_bulan = "Oktober";
                    break;
                case 11:
                    nama_bulan = "November";
                    break;
                case 12:
                    nama_bulan = "Desember";
                    break;
            }
            return tanggal + ' ' + nama_bulan + ' ' + tahun;
        }
    }
}
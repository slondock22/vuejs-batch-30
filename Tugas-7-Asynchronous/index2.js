var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

function baca(remainingTime, books, indeks) {
    readBooksPromise(remainingTime, books[indeks]).then(function(time) {
        let nextBook = indeks + 1;
        if (nextBook < books.length) {
            baca(time, books, nextBook);
        }
    });
}

baca(10000, books, 0);
//Soal 1
let panjang = 20;
let lebar = 30;

const kelPersegiPanjang = (panjang, lebar) => {
    let keliling = 0;

    keliling = 2*(panjang+lebar)
    return keliling
}

console.log(kelPersegiPanjang(panjang,lebar))

//Soal 2
const newFunction = (firstName, lastName) => ({
        firstName : firstName,
        lastName : lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
});

newFunction("William", "Imoh").fullName() 

//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName,lastName,address,hobby} = newObject;

console.log(firstName, lastName, address, hobby);

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [west,east]

console.log(combined)

//Soal 5
const planet = "earth" 
const view = "glass" 

const  theString =  `Lorem ${view} dolor sit amet, consectrtur adipiscing elit ${planet}`;
console.log(theString);
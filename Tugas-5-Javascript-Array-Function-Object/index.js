//Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sortDaftarHewan = daftarHewan.sort();

for(var i=0; i < daftarHewan.length; i++){
    console.log(sortDaftarHewan[i]+'\n')
}

//Soal 2
function introduce(arrdata){
    return 'Nama Saya '+arrdata.name+', umur saya '+arrdata.age+', alamat saya di '+arrdata.address+', dan saya punya hobby yaitu '+arrdata.hobby;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) 

//Soal 3

function hitung_huruf_vokal(kata) {
  var huruf_vokal = 'aeiouAIUEO';
  var hitung = 0;
  
  for(var x = 0; x < kata.length ; x++)
  {
    if (huruf_vokal.indexOf(kata[x]) !== -1)
    {
      hitung += 1;
    }
  
  }
  return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

//Soal 4
function hitung(angka) {
    var hasil = 0;

    hasil = angka + (angka - 2);

    return hasil;
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

